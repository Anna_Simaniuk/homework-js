//1. Цикл - це спосіб повторити один і той же код декілька разів
//2. Якщо умова циклу є істиною і сам по собі цикл простий, то можна використовувати while. Проте, якщо потрібно прописати змінну циклу, а також правила її зміни після виконання циклу то потрібно використовувати for.
//3. Коли ти свідомо хочеш перетворити один тип в інших використовуючи, наприклад Number(value), це явне перетворення типу. Коли ти використовуєш різні оператори, які своєю чергою автоматично змінюють тип, це неявне перетворення типу (наприклад 1+'one')

let userNumber = +prompt('Enter number', '');

while (!Number.isInteger(userNumber) === true) {
    userNumber = +prompt('Enter number', '');
}

for (let number = 0; number <= userNumber; number++) {
    if (userNumber < 5) console.log('Sorry, no numbers');
    if (userNumber < 5) break;
    if (number % 5 !== 0) continue;
    console.log(number);
}

// доп завдання

alert('Зараз Вам потрібно ввести 2 числа')
let m = +prompt('Введіть менше число', '');
let n = +prompt('Введіть більше число', '');

while (m >= n) {
    alert('Помилка, перше число повинно бути меншим за друге!!!');
    m = +prompt('Введіть менше число', '');
    n = +prompt('Введіть більше число', '');
}

next:
    for (m; m <= n; m++) {
        if (m < 2) continue;
        for (let j = 2; j < m; j++) {
            if (m % j === 0) continue next;
        }
        console.log(m)
    }


