'use strict';

const body = document.querySelector('body');
const btn = document.createElement('button');
btn.textContent = 'Change theme';
btn.classList.add('btn_change_theme');
document.querySelector('.page_one').prepend(btn);

btn.addEventListener('click', () => {
    body.classList.toggle('theme_dark');
    if (body.classList.contains('theme_dark')) {
        localStorage.setItem('theme', 'theme_dark');
    } else {
        localStorage.setItem('theme', 'theme_light');
    }
});

const thisTheme = localStorage.getItem('theme')
if (thisTheme === 'theme_dark') {
    body.classList.add(`${thisTheme}`);
}

