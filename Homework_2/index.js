//1. Існує 6 примітивних типів даних в JS: undefined, number, string, boolean, symbol, BigInt. Окремо виділяють Null i Object.
//2. Різниця між == та === полягає в тому, що при першому варіанті не проводиться перевірка на тип даних
//3. Оператори використовуються для управління потоком команд в JS. Можуть бути: присвоєння, логічні, арифметичні, порівняння і т.д.

let name = prompt('What\'s your name?', '');
let age = prompt('How old are you?', '');

while (!isNaN(name)) {
    name = prompt('What\'s your name?', `${name}`);
}

while (isNaN(age)) {
    age = prompt('How old are you?', `${age}`);
}

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age <= 22) {
    const userAge = confirm('Are you sure you want to continue?');

    if (userAge) {
        alert(`Welcome, ${name}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${name}`);
}