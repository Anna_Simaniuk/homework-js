//Тому що події клавіатури передбачені для роботи з клавіатурою, а в інпут ми можемо вставити текст через контекстне меню через праву кнопку мишки, таким чином ми нічого не побачимо.

const btn = document.querySelectorAll('.btn');

btn.forEach(el => {
    document.addEventListener('keydown', e => {
        el.textContent.toLowerCase() === e.key.toLowerCase() ? el.classList.add('blue') : el.classList.remove('blue');
    })
})