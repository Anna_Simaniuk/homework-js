'use strict';
// DOM - весь контент сторінки, який ми можемо змінювати.
// innerText вставляє текст в документ як текст, не розпізнаючи html тегів, на відміну від innerHTML.
// Звернутись до елементів ми можемо за допомогою querySelector*, getElementBy*. Кращий querySelector, тому що ми можемо отримати доступ до елемента по всім CSS селекторам.

const allParagraph = document.querySelectorAll('p');
const optionsList = document.getElementById('optionsList');
const mainHeader = document.querySelector('.main-header')
const sectionTitle = document.querySelectorAll('.section-title')

allParagraph.forEach((element) => {
    element.style.backgroundColor = '#ff0000';
})

console.log(optionsList);
console.log(optionsList.parentElement);

optionsList.childNodes.forEach((element) => console.log(element.nodeName, element.nodeType))

document.getElementById('testParagraph').innerHTML = 'This is a paragraph';

console.log(mainHeader.children);
mainHeader.childNodes.forEach((el) => el.nodeType === 1 && el.classList.add('nav-item'))

sectionTitle.forEach((element) => element.classList.remove('section-title'))








