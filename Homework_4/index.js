// Функції потрібні для того, щоб була можливість використовувати один і той самий код декілька разів без повторення самого коду.
// Для того, щоб наша функція підставляла ті значення в параметр, які нам необхідні для виконання. Аргументи кожного разу можуть бути різними при виклику функції.
// Оператор return повертає значення функції та припиняє її виконання.

const app = document.getElementById('app')
const inputFirstNumber = app.querySelector('#firstNumber');
const inputSecondNumber = app.querySelector('#secondNumber');
const selectOperation = app.querySelector('#operation');
const btnCalc = app.querySelector('#calc');

const calc = () => {
    btnCalc.addEventListener('click', () => {
        let check = (!!inputFirstNumber.value && !!inputSecondNumber.value && !isNaN(inputFirstNumber.value) && !isNaN(inputSecondNumber.value));
        if (!!check) {
                const operations = {
                    '+': +inputFirstNumber.value + +inputSecondNumber.value,
                    '-': +inputFirstNumber.value - +inputSecondNumber.value,
                    '*': +inputFirstNumber.value * +inputSecondNumber.value,
                    '/': +inputFirstNumber.value / +inputSecondNumber.value,
                };
                document.getElementById('result').value = operations[selectOperation.value];
        } else {
            alert('Введіть дані наново');
        }
    });
};

calc();
