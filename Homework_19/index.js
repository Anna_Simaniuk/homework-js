function deadline(storePointTeam, storePointNeed, deadline) {
    let teamResultForOneDay = storePointTeam.reduce((sum, current) => sum + current, 0);
    let allNeedStorePoint = storePointNeed.reduce((sum, current) => sum + current, 0);
    let deadlineDay = Math.ceil((new Date(deadline) - new Date()) / 86400000);
    let dayNeedToTeam = Math.ceil(allNeedStorePoint / teamResultForOneDay);
    if (dayNeedToTeam < deadlineDay) {
        return alert(`Усі завдання будуть успішно виконані за ${deadlineDay - dayNeedToTeam} днів до настання дедлайну!`)
    } else if (dayNeedToTeam === deadlineDay) {
        alert('Вітаю! Ви вклались в дедлайн')
    } else {
        let hourAfterDeadline = (allNeedStorePoint - (teamResultForOneDay * deadlineDay))*8/teamResultForOneDay;
        return alert(`Команді розробників доведеться витратити додатково ${hourAfterDeadline} годин після дедлайну, щоб виконати всі завдання в беклозі`);
    }
}

deadline();
