// Екранування - це перетворення спец. символа на звичайний. Відбувається, якщо додати до спец. символів \. Воно потрібне для можливості використання спец.символів в рядочку.
// Function Declaration, Function Expression, Arrow functions
// Хостинг переміщує всі оголошення на початок поточної області. При написанні Function Declaration у нас є можливість викликати функцію до її оголошення.

function createNewUser() {
    const newUser = {
        firstName: prompt('Enter your first name', '').trim(),
        lastName: prompt('Enter your last name', '').trim(),
        birthday: prompt('Enter your birthday (dd.mm.yyyy)', ''),
        getLogin() {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}`;
        },
        getAge() {
            this.birthday =  new Date(this.birthday.replace(/(\d{2}).(\d{2}).(\d{4})/, '$3.$2.$1'));
            const thisYear = new Date().getFullYear();
            return `${thisYear}` - `${this.birthday.getFullYear()}`;
        },
        getPassword() {
            return `${this.getLogin()}${this.birthday.getFullYear()}`
        },
        setFirstName() {
            Object.defineProperty(user, 'firstName', {writable: true})
        },
        setLastName() {
            Object.defineProperty(user, 'lastName', {writable: true})
        },
    }
    return newUser;
}

let user = createNewUser();

Object.defineProperties(user, {
        firstName: {writable: false},
        lastName: {writable: false},
    }
)

user.setFirstName();
user.setLastName();

console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
