// Метод forEach дозволяє перебрати всі елементи в масиві. Не змінює значення та не повертає результат.
// 1. Можна змінити length на 0; 2. Можна використати метод splice
// Метод Array.isArray або викликати функцію Object.prototype.toString.call, яка поверне значення [object Array], якщо елемент є масивом

const filterBy = (arr, type) => arr.filter((value) => typeof value !== type);
