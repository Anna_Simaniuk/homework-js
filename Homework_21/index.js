'use strict';

const drawCircle = document.querySelector('.draw_circle');

const diameter = document.createElement('input');
diameter.setAttribute('placeholder', 'Введіть діаметр');

const draw = document.createElement('button');
draw.textContent = 'Намалювати';

const wrapperCircle = document.createElement('div');
wrapperCircle.classList.add('wrapper-circle');

drawCircle.addEventListener('click', () => {
    drawCircle.after(draw);
    drawCircle.after(diameter);
    drawCircle.style.display = 'none';
})

const randomColor = () => {
    const values = '0123456789ABCDEF';
    let hash = '#';
    for (let i = 0; i < 6; i++) {
        hash += values[Math.floor(Math.random() * 16)];
    }
    return hash;
}

draw.addEventListener('click', () => {
    draw.after(wrapperCircle);
    for (let i = 0; i < 100; i++) {
        wrapperCircle.insertAdjacentHTML('beforeend', '<div class="circle"></div>');
    }
    const circle = document.querySelectorAll('.circle');
    circle.forEach(el => {
        el.style.width = `${diameter.value}px`;
        el.style.height = `${diameter.value}px`;
        el.style.borderRadius = '50%';
        el.style.background = `${randomColor()}`;
    })

    wrapperCircle.addEventListener('click', e => {
        if (e.target.classList.contains('circle')) {
            e.target.style.display = 'none';
        }
    })
})



