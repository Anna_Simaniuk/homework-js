'use strict';

const display = document.querySelector('.display-input');
const keys = document.querySelector('.keys');
const displayInput = document.querySelector('.display-input');
let optionsButtonPress = false;
let operator;
let firstNumber;
let secondNumber;
let result;
let saveNumber;
let countSaveNumber = 0;

displayInput.insertAdjacentHTML('beforebegin', '<span class="save_number">m</span>');

function matchOperations(firstNumber, secondNumber, operator) {
    const operation = {
        '+': firstNumber + secondNumber,
        '-': firstNumber - secondNumber,
        '*': firstNumber * secondNumber,
        '/': firstNumber / secondNumber,
    }
    return String(operation[operator]);
}

keys.addEventListener('click', e => {
    if (e.target.classList.contains('button')) {
        if (e.target.classList.contains('black') && e.target.value !== 'C') numberPressed(e.target.value);

        if (e.target.value === 'C') nullValue();

        if (e.target.classList.contains('pink')) symbolPressed(e.target.value);

        if (e.target.classList.contains('orange') && firstNumber && display.value && operator) equalPressed();

        if (e.target.value === 'm+' || e.target.value === 'm-') {
            saveNumber = display.value;
            document.querySelector('.save_number').style.display = 'inline-block';
        }

        if (e.target.value === 'mrc') {
            if (countSaveNumber === 0) {
                display.value = saveNumber;
                countSaveNumber++
            } else {
                saveNumber = '';
                countSaveNumber = 0;
                document.querySelector('.save_number').style.display = 'none';
            }
        }
    }
})

function numberPressed(e) {
    if (!!optionsButtonPress) {
        display.value = '';
        secondNumber += e;
        display.value += secondNumber;
    } else {
        firstNumber += e;
        display.value += e;
    }
}

function nullValue() {
    display.value = '';
    firstNumber = '';
    secondNumber = '';
    operator = '';
    result = '';
    optionsButtonPress = false;
}

function symbolPressed(e) {
    if (firstNumber && display.value && operator) {
        result = matchOperations(Number(firstNumber), Number(secondNumber), operator);
        firstNumber = result;
        secondNumber = '';
        display.value = firstNumber;
        operator = e;
        optionsButtonPress = true;
    } else {
        firstNumber = display.value;
        operator = e;
        secondNumber = '';
        optionsButtonPress = true;
    }
}

function equalPressed() {
    result = matchOperations(Number(firstNumber), Number(secondNumber), operator);
    firstNumber = result;
    display.value = result;
    secondNumber = '';
    operator = '';
    optionsButtonPress = true;
}

//keydown
const numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
const symbols = ['+', '-', '/', '*'];
window.addEventListener('keydown', (e) => {
    if (numbers.includes(e.key)) numberPressed(e.key);

    if (symbols.includes(e.key)) symbolPressed(e.key);

    if (e.key === 'Enter' && firstNumber && display.value && operator) equalPressed();

    if (e.key.toLowerCase() === 'c' || e.key === 'Delete') nullValue();
})