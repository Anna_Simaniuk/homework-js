const n = prompt('Введіть номер числа Фібоначчі, яке ви кочете знайти?', '');

function fibonacci(n) {
    let f0 = 0;
    let f1 = 1;
    let f2;
    if (n > 0) {
        for (let i = 2; i <= n; i++) {
            f2 = f0 + f1;
            f0 = f1;
            f1 = f2;
        }
    } else {
        for (let i = -2; i >= n; i--) {
            f2 = f0 - f1;
            f0 = f1;
            f1 = f2;
        }
    }
    return f2;
}

console.log(fibonacci(n));
