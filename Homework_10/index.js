const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content');
const tabsTitles = document.querySelectorAll('.tabs-title');
const tabContent = Array.from(tabsContent.children);

for (const tabsTitle of tabsTitles) {
    tabsTitle.setAttribute('data-name', `${tabsTitle.textContent}`);
}
tabsTitles[0].classList.add ('active')

const comments = () => {
    const arrComment = [];
    for (const tabContent of tabsContent.childNodes) {
        if (tabContent.nodeType === 8) {
            arrComment.push(tabContent);
        }
    }
    return arrComment;
}

comments().forEach((commentValue, commentIndex) => {
    tabContent.forEach((contentValue, contentIndex) => {
        if (commentIndex === contentIndex) {
            contentValue.setAttribute('data-name', `${commentValue.nodeValue.trim()}`)
            contentValue.style.display = 'none';
        }
        tabContent[0].style.display = 'list-item';
    })
})


tabs.addEventListener('click', e => {
    if (e.target.classList.contains('tabs-title')) {
        for (const tab of tabsTitles) {
            tab.classList.remove('active')
        }
        e.target.classList.add('active')

        tabContent.forEach((el => {
            if (e.target.dataset.name === el.dataset.name) {
                el.style.display = 'list-item';
            } else {
                el.style.display = 'none';
            }
        }))
    }
})