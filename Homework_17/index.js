const student = {};

student.name = prompt('Введіть ваше ім\'я', '');
student.lastName = prompt('Введіть ваше прізвище', '');
student.tabel = {};

while (true) {
    const subject = prompt('Введіть назву предмета', '');
    if (!subject) {
        break
    }
    const mark = +prompt('Введіть оцінку', '');
    student.tabel[subject] = mark;
}

let badMaks = 0;

for (const key in student.tabel) {
    if (student.tabel[key] < 4) {
        badMaks++;
    }
}
if (badMaks === 0) {
    alert('Студент переведено на наступний курс');
}

let averageScore = 0;
let marks = 0;
let marksScore = 0;

for (const key in student.tabel) {
    marksScore++;
    marks += student.tabel[key];
    averageScore = marks / marksScore;
}
if (averageScore > 7) {
    alert('Студенту призначено стипендію');
}

console.log(student);

