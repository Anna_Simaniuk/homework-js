// Рекурсія - виклик функції самої себе. Приклад рекурсії наведено в завданні нижче.

let number = +prompt('Введіть число', '0');

while (isNaN(number)) {
    number = +prompt('Введіть число', '0');
}

function factorial(userNumber) {
    if (userNumber === 0) {
        return 1;
    } else {
        return userNumber * factorial(userNumber - 1);
    }
}

alert('Факторіал вашого числа ' + `${factorial(number)}`);
