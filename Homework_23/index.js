// Обробник подій - це функція, яка може реагувати на дії користувача та спрацює, коли подія трапилась
// Обробник можна додати в розмітку (атрибут) або використовуючи властивість DOM-елемента on<event>, або ж найкращий спосіб addEventListener, перевагою є те що можна повісити декілька обробників на один елемент.

'use strict';

const priceValue = document.querySelector('.price-value');

const currentPrice = document.createElement('span');
currentPrice.classList.add('current-price')

const error = document.createElement('div');
error.classList.add('error')

priceValue.addEventListener('focus', () => {
    priceValue.style.border = '1px solid limegreen';
    priceValue.style.color = 'black';
})

priceValue.addEventListener('change', () => {
    if (+priceValue.value < 0) {
        priceValue.addEventListener('blur', () => priceValue.style.border = '1px solid red');
        priceValue.after(error);
        priceValue.style.color = 'black';
        error.textContent = 'Please enter correct price'
        currentPrice.style.display = 'none';
    } else {
        priceValue.addEventListener('blur', () => priceValue.style.border = '1px solid black');
        priceValue.after(currentPrice);
        currentPrice.textContent = `Current price: ${priceValue.value}`
        currentPrice.style.display = 'inline';
        priceValue.style.color = 'limegreen';
        error.textContent = '';
        currentPrice.insertAdjacentHTML('beforeend', '<span class="delete">&#10007;</span>')

        currentPrice.addEventListener('click', e => {
            if (e.target.classList.contains('delete')) {
                currentPrice.style.display = 'none';
                priceValue.value = '';
            }
        })
    }
})


