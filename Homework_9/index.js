'use strict';
// document.createElement(tag) – створює елемент із заданим тегом;
/* перший параметр означає куди саме потрібно вставити фрагмент html. Буває:
 `beforebegin' - вставляє html прямо перед elem,
 `afterbegin' - вставляє html в elem на початок,
 `beforeend' - вставляє html в elem в кінець,
 `afterend' - вставляє html відразу після elem. */
// node.remove() або parent.removeChild(node)

const displayElements = (arr, parentArg = 'body') => {
    const place = document.querySelector(parentArg);
    const list = document.createElement('ul');
    place.append(list);
    arr.map((value) => {
        if (!Array.isArray(value)) {
            list.insertAdjacentHTML("beforeend", `<li>${value}</li>`);
        } else {
            const newList = displayElements(value);
            list.append(newList);
        }
    })
    return list
}

displayElements(["Kharkiv", ["Kiev", ["Borispol", ["Irpin", ["Odessa", ["Lviv", ["Dnieper"]]]]]]], 'div')

const timer = () => {
    let second = 3;
    const div = document.createElement('div');
    document.body.append(div)
    div.style.textAlign = 'center';
    div.textContent = `Вікно очиститься через ${second} сек.`;
    setInterval(function () {
        if (second > 1) {
            second--;
            div.textContent = `Вікно очиститься через ${second} сек.`;
        } else {
            clearInterval(timer);
            document.body.hidden = true
        }
    }, 1000)
}
timer();

