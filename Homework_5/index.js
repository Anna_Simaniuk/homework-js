// Метод об'єкту це функція, яка є властивістю об'єкта.
// Будь-яке.
// Те, що змінна об'єкта зберігає в собі посилання на значення самого об'єкта. Тобто при копіюванні буде копіюватись посилання на місце де зберігається об'єкт, а не сам об'єкт.

function createNewUser() {
    const newUser = {
        firstName: prompt('Enter your first name', '').trim(),
        lastName: prompt('Enter your last name', '').trim(),
        getLogin() {
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        },
        setFirstName() {
            Object.defineProperty(user, 'firstName', {writable: true})
        },
        setLastName() {
            Object.defineProperty(user, 'lastName', {writable: true})
        },
    }
    return newUser;
}

let user = createNewUser();

Object.defineProperties(user, {
        firstName: {writable: false},
        lastName: {writable: false},
    }
)

user.setFirstName();
user.setLastName();

console.log(user.getLogin());
