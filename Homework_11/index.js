'use strict';

const form = document.querySelector('.password-form');
const inputWrapper = Array.from(document.querySelectorAll('.input-wrapper'))
const password = Array.from(document.querySelectorAll('input[type="password"]'));
const eye = document.querySelectorAll('.fa-eye');
const eyeSlash = document.querySelectorAll('.fa-eye-slash');
const btn = document.querySelector('.btn');

const error = document.createElement('div');
error.style.cssText = `
color : red;
font-size : 13px;
margin-bottom: 6px;
display : none;
`;
inputWrapper[1].after(error)

eyeSlash.forEach((eye) => eye.style.zIndex = '-1');

function showPassword(el) {
    el.addEventListener('click', e => {
        e.target.style.zIndex = '-1';
        if (e.target.classList.contains('fa-eye')) {
            e.target.nextElementSibling.style.zIndex = '0';
            const password = e.target.previousElementSibling
            password.setAttribute('type', 'text');
        } else {
            e.target.previousElementSibling.style.zIndex = '0';
            const password = e.target.previousElementSibling.previousElementSibling
            password.setAttribute('type', 'password');
        }
    })
}

eye.forEach(el => showPassword(el));

eyeSlash.forEach(el => showPassword(el));


// Перевірка паролю

const listWrapper = document.createElement('ul');
listWrapper.textContent = 'Пароль повинен містити:';
listWrapper.style.margin = '0 0 20px';
listWrapper.style.listStyleType = 'none';
inputWrapper[0].after(listWrapper);

listWrapper.insertAdjacentHTML("beforeend", '<li>Не менше 8 символів</li>');
listWrapper.insertAdjacentHTML("beforeend", '<li>Маленьку літеру</li>');
listWrapper.insertAdjacentHTML("beforeend", '<li>Велику літеру</li>');
listWrapper.insertAdjacentHTML("beforeend", '<li>Цифру</li>');
listWrapper.insertAdjacentHTML("beforeend", '<li>Спец. символ</li>');

for (const list of listWrapper.children) {
    list.classList.add('list');
}

const list = document.querySelectorAll('.list')

function checkPassword() {
    const check = password[0].value
    check.length >= 8 ? list[0].classList.add('done') : list[0].classList.remove('done');
    /[a-zа-я]/.test(check) ? list[1].classList.add('done') : list[1].classList.remove('done');
    /[A-ZА-Я]/.test(check) ? list[2].classList.add('done') : list[2].classList.remove('done');
    /\d/.test(check) ? list[3].classList.add('done') : list[3].classList.remove('done');
    /[\[\]\\^$.|?*+()\/!@#%&\-_={},<>;:`~'"]/.test(check) ? list[4].classList.add('done') : list[4].classList.remove('done');
}

password[0].addEventListener('change', checkPassword)


// Взаємодія з кнопкою

btn.addEventListener('click', (e) => {
    e.preventDefault()
    if (document.querySelectorAll('.done').length === list.length) {
        if (password[0].value === password[1].value && password[0].value !== '' && password[1].value !== '') {
            alert('Вітаю! Ти ввів правильно пароль! \u{1F60E}')
        } else {
            password[1].style.marginBottom = '2px';
            error.style.display = 'block';
            error.textContent = 'Схоже, щось пішло не так. Паролі не співпадають!';
        }
    } else {
        password[1].style.marginBottom = '2px';
        error.style.display = 'block';
        error.textContent = 'Твій пароль не відповідає вимогам!'
    }
})


password.forEach((el) => {
    el.addEventListener('focus', () => {
        password[1].style.marginBottom = '24px';
        error.style.display = 'none'
    })
})