function deepCopyObject(obj) {
    const copyObj= {};
    for (const element in obj) {
        if (typeof (obj[element]) === 'object') {
            copyObj[element] = deepCopyObject(obj[element]);
            continue;
        }
        copyObj[element] = obj[element];
    }
    return copyObj;
}

const copyObj = deepCopyObject()
