'use strict';

const table = document.createElement('table');
document.body.prepend(table)
const tbody = document.createElement('tbody')
tbody.classList.add('white')
table.append(tbody)

for (let i = 0; i < 30; i++) {
    const tr = document.createElement('tr');
    for (let j = 0; j < 30; j++) {
        const td = document.createElement('td');
        tr.append(td)
    }
    tbody.append(tr);
}

const tdArr = Array.from(document.getElementsByTagName('td'))

document.body.addEventListener('click', e => {
    if (e.target.matches('td')) {
        e.target.classList.toggle('black')
    } else if (!e.target.matches('table')){
        tdArr.forEach(td => td.classList.toggle('black'))
    }
});

