'use strict';
function filterCollection(arr, value, boolean, ...keys) {
    const valueArr = value.split(' ');
    let arrFilterValue;


    arrFilterValue = arr.filter(obj => {
        let objValues = [];

        keys.forEach(key => {
            let currentValue = obj;
            const keyChain = key.split('.');

            keyChain.forEach(keyPart => {
                currentValue = currentValue[keyPart];
            });

            objValues.push(currentValue);
        });

        if (!!boolean){
            return valueArr.every(valueEl => objValues.includes(valueEl))
        } else {
            return valueArr.some(valueEl => objValues.includes(valueEl))
        }
    })
    return arrFilterValue;
}


let users = [
    {name: 'Anna', surname: 'Simaniuk', age: '28', address: {street: 'Sechenova', place:{house: '25', flat: '27'}}},
    {name: 'Olha', surname: 'Shchavinska', age: '24', address: {street: 'Komarova', place:{house: '31', flat: '45'}}},
    {name: 'Vova', surname: 'Simaniuk', age: '31', address: {street: 'Sechenova', place:{house: '25', flat: '27'}}},
    {name: 'Oksana', surname: 'Shchavinska', age: '52', address: {street: 'Yshka', place:{house: '28', flat: '75'}}},
    {name: 'Viktor', surname: 'Shchavinskii', age: '58', address: {street: 'Yshka', place:{house: '5', flat: '28'}}},
    {name: 'Anna', surname: 'Moisuk', age: '76', address: {street: 'Miry', place:{house: '1', flat: '25'}}},
]

console.log(filterCollection(users, 'Anna 75 Sechenova', false, 'name', 'age', 'address.place.flat', 'address.street'));