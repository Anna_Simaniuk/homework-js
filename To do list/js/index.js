'use strict';
import('./second part.js');

const inputsWrapper = document.querySelector('.inputs_wrapper')

const inputsWrapperButton = document.createElement('div');
inputsWrapperButton.classList.add('inputs_wrapper_button');
inputsWrapper.append(inputsWrapperButton);

const toDoListLabel = document.createElement('label');
toDoListLabel.classList.add('to_do_list_label');
inputsWrapperButton.prepend(toDoListLabel);

const toDoListCheckbox = document.createElement('input');
toDoListCheckbox.classList.add('to_do_list_checkbox');
toDoListCheckbox.setAttribute('type', 'checkbox');
toDoListCheckbox.setAttribute('disabled','');
toDoListLabel.prepend(toDoListCheckbox)

const toDoListText = document.createElement('div');
toDoListText.classList.add('to_do_list_text')
toDoListLabel.append(toDoListText);

const toDoListContent = document.createElement('div');
toDoListContent.classList.add('to_do_list_content')
toDoListText.prepend(toDoListContent);

const toDoListInput = document.createElement('input');
toDoListInput.classList.add('to_do_list_input')
toDoListText.append(toDoListInput);

const pencil = document.createElement('i');
pencil.classList.add('fas')
pencil.classList.add('fa-pencil-alt')
pencil.classList.add('pencil')
pencil.setAttribute('title','Edit')
inputsWrapperButton.append(pencil)

const buttonSave = document.createElement('button');
buttonSave.textContent = 'Save';
buttonSave.classList.add('button_save');
buttonSave.classList.add('button');
inputsWrapperButton.append(buttonSave);

const buttonAddNewList = document.createElement('button');
buttonAddNewList.textContent = 'Add new list';
buttonAddNewList.classList.add('button');
buttonAddNewList.classList.add('add_new_list');
inputsWrapper.prepend(buttonAddNewList);


buttonAddNewList.addEventListener('click', () => {
    inputsWrapper.insertAdjacentHTML("beforeend", '<div class="inputs_wrapper_button">' +
        '<label class="to_do_list_label">' +
        '<input class="to_do_list_checkbox" type="checkbox" disabled>' +
        '<div class="to_do_list_text"><div class="to_do_list_content">' +
        '</div><input class="to_do_list_input"></div></label>' +
        '<i class="fas fa-pencil-alt pencil" aria-hidden="true" title="Edit"></i>' +
        '<button class="button_save button">Save</button></div>')
});


inputsWrapper.addEventListener('click', (e) => {
    if (!e.target.classList.contains('button_save')) {
        return
    }
    const parentNode = e.target.parentNode;
    const childLabel = parentNode.childNodes[0];
    const checkbox = childLabel.childNodes[0];
    const text = childLabel.childNodes[1];
    const content = text.childNodes[0];
    const input = text.childNodes[1];

    content.textContent = input.value;
    input.style.zIndex = '-2';
    checkbox.removeAttribute('disabled')
})


inputsWrapper.addEventListener('click', (e) => {
    if (!e.target.classList.contains('pencil')) {
        return
    }
    const parentNode = e.target.parentNode;
    const childLabel = parentNode.childNodes[0];
    const checkbox = childLabel.childNodes[0];
    const text = childLabel.childNodes[1];
    const input = text.childNodes[1];

    input.style.zIndex = '1';
    checkbox.setAttribute('disabled', '')
})

