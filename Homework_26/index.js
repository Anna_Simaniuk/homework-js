'use strict';

const btnPrevious = document.querySelector('.previous');
const btnNext = document.querySelector('.next');
const viewport = document.querySelector('.viewport ').offsetWidth;
const wrapperSlider = document.querySelector('.wrapper_slider');
let viewSlide = 0;

btnNext.addEventListener('click', () => {
    viewSlide < 5 ? viewSlide++ : viewSlide = 0;
    wrapperSlider.style.left = -viewSlide * viewport + 'px';
})

btnPrevious.addEventListener('click', () => {
    viewSlide > 0 ? viewSlide-- : viewSlide = 5;
    wrapperSlider.style.left = -viewSlide * viewport + 'px';
})

