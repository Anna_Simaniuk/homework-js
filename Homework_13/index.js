// Функція setInterval дає можливість багаторазово запускати функцію.
// Вона спрацює миттєво (без затримки), але після виконання усього скрипту.
// Тому що навіть якщо ми вже не бачимо результату виконання функції, при setInterval вона і далі продовжує викликатись, що призводить до засмічення (напр. параметри, які могли бути стертими, через відсутність необхідності в них, не витираються).

'use strict';

const images = document.querySelectorAll('.image-to-show')
images.forEach(img => img.style.display = 'none');

const btnStop = document.createElement('button');
btnStop.textContent = 'STOP';
btnStop.classList.add('btn');
document.body.append(btnStop);

const btnPlay = document.createElement('button');
btnPlay.textContent = 'PLAY';
btnPlay.classList.add('btn');
document.body.append(btnPlay);

let second = 2;
let miliSecond = 100;

const div = document.createElement('div');
div.textContent = `До наступної картинки залишилось ${second}сек.`;

const span = document.createElement('span');
span.classList.add('milisec')
span.textContent = `${miliSecond} мілісек.`

document.body.append(div);
document.body.append(span);


//анімація

const fadeIn = (el) => {
    el.style.opacity = '0';
    el.style.display = 'inline-block';
    el.style.transition = 'opacity 0.5s';
    setTimeout(() => {
        el.style.opacity = '1';
    }, 500);
};


const fadeOut = (el) => {
    el.style.opacity = '1';
    el.style.transition = 'opacity 0.5s';
    let fadeOutTimer = setTimeout(() => {
        el.style.opacity = '0';
    }, 2500);
    btnStop.addEventListener('click', () => {
        clearTimeout(fadeOutTimer)
    })
};


// зміна зображення

function changeImg(index) {
    fadeOut(images[index - 1])
    fadeIn(images[index - 1])

    function timerImg() {
        images.forEach(img => img.style.display = 'none')
        if (index > images.length - 1) {
            index = 1;
            changeImg(index)
        } else {
            fadeOut(images[index])
            fadeIn(images[index])
            index++;
        }
    }

    let timer = setInterval(timerImg, 3000);

    btnStop.addEventListener('click', () => {
        clearInterval(timer);
        clearInterval(timerSecond);
        clearInterval(timerMiliSec);
    })

    btnPlay.addEventListener('click', () => {
        images.forEach(img => img.style.display = 'none');
        fadeOut(images[index - 1]);
        fadeIn(images[index - 1]);
        timer = setInterval(timerImg, 3000);
    })
}

changeImg(1);

// секундомір

function sec() {
    div.textContent = `До наступної картинки залишилось ${--second}сек.`;
    if (second < 0) {
        second = 3;
        sec()
    }
}

function milisec() {
    span.textContent = `${--miliSecond} мілісек.`
    if (miliSecond < 0) {
        miliSecond = 100;
        milisec();
    }
}

let timerSecond = setInterval(sec, 1000);
let timerMiliSec = setInterval(milisec, 10);

btnPlay.addEventListener('click', () => {
    second = 2;
    div.textContent = `До наступної картинки залишилось ${second}сек.`;
    miliSecond = 100;
    span.textContent = `${miliSecond} мілісек.`
    timerSecond = setInterval(sec, 1000);
    timerMiliSec = setInterval(milisec, 10);
})
